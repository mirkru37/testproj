﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour
{
    public GameObject Pause;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play(GameObject menu)
    {
        Player_Controler.isPlaying = true;
        menu.SetActive(false);
        Pause.SetActive(true);
    }

    public void setPause(GameObject menu)
    {
        Player_Controler.isPlaying = false;
        menu.SetActive(true);
        Pause.SetActive(false);
    }
}
