﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;
using UnityEngine.UI;
using System;
using UnityEditor;

public class Player_Controler : MonoBehaviour
{
    public static bool isPlaying;
    public float movementSpeed = 1;
    public float jumpForce = 1;
    public LayerMask whatIsGround;
    private Rigidbody2D rb;
    public Animator anim;
    public bool isGround;
    private BoxCollider2D _Cl;
    void Start()
    {
        isPlaying = false;
        rb = GetComponent<Rigidbody2D>();
        _Cl = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

        isGround = Physics2D.IsTouchingLayers(_Cl, whatIsGround);
        if (isGround)
        {
            anim.SetBool("onGraund", true);
        }
        else
        {
            anim.SetBool("onGraund", false);
        }

        if (isPlaying)
        {
            var movement = Input.GetAxis("Horizontal");
            transform.position += new Vector3(movement, 0, 0) * (Time.deltaTime * movementSpeed);
            anim.SetFloat("Speed", Mathf.Abs(movement));

            if (!Mathf.Approximately(0, movement))
            {
                transform.rotation = movement < 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
            }

            if (Input.GetButtonDown("Jump") && Mathf.Abs(rb.velocity.y) < 0.001f)
            {
                rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
                // anim.SetTrigger("jump"); 
            }
        }

    }
}
